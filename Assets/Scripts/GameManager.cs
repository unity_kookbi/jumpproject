using System.IO;
using UnityEngine;
using Newtonsoft.Json;

public class GameManager : MonoBehaviour
{
    /// <summary>
    /// 생성된 단 하나의 Gamemanager 객체를 나타낼 변수
    /// </summary>
    private static GameManager _Instance;

    public static GameManager getInstance => _Instance;

    /// <summary>
    /// 최고 점수 데이터
    /// </summary>
    public ScoreData m_BestScoreData;

    /// <summary>
    /// 현재 점수 데이터
    /// </summary>
    public ScoreData m_ScoreData;

    private void Awake()
    {

        if ( _Instance == null)
        {
            _Instance = this;

            LoadBestScoreData();

            m_ScoreData = new();
        }
        else
        {
            Destroy(gameObject);
        }

        // 씬이 변경되어도 이 오브젝트는 사라지지 않도록 합니다.
        DontDestroyOnLoad(gameObject);

    }


    /// <summary>
    /// 최고 점수를 갱신합니다.
    /// </summary>
    public void UpdateBestScore()
    {
        // 최고 점수보다 현재 점수가 더 높다면
        if(m_BestScoreData.m_Score < m_ScoreData.m_Score)
        {
            // 최고 점수 기록
            m_BestScoreData.m_Score = m_ScoreData.m_Score;

            SaveBestScoreData();
        }

    }

    /// <summary>
    /// 최고 점수 파일을 로드합니다.
    /// </summary>
    /// <returns></returns>
    public bool LoadBestScoreData()
    {
        try
        {
            // 파일의 내용을 읽습니다.
            string readText = File.ReadAllText(Constants.SAVE_DATA_PATH + Constants.SAVE_DATA_FILENAME);

            // 역직렬화
            m_BestScoreData = JsonConvert.DeserializeObject<ScoreData>(readText);
        }
        catch(DirectoryNotFoundException)
        {
            m_BestScoreData = new();
        }
        catch(FileNotFoundException)
        {

            m_BestScoreData = new();
        }

        return false;
    }

    /// <summary>
    /// 최고 점수 파일을 저장합니다.
    /// </summary>
    public void SaveBestScoreData()
    {
        // 경로를 생성합니다.
        Directory.CreateDirectory(Constants.SAVE_DATA_PATH);

        // 직렬화
        string toJson = JsonConvert.SerializeObject(m_BestScoreData);

        // 파일 쓰기
        File.WriteAllText(
            Constants.SAVE_DATA_PATH + Constants.SAVE_DATA_FILENAME,
            toJson);

    }


    public void ResetScoreData()
    {
        m_ScoreData = new();
    }
}

