

/// <summary>
/// 사용될 색상 타입을 나타내기 위한 열거 형식입니다.
/// </summary>
public enum ColorType : sbyte
{
    Red         = 0,
    Orange      = 1,
    Yellow      = 2,
    Green       = 3,
    Blue        = 4,
    Magenta     = 5,
    White       = 6
}