using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class LineGroup : MonoBehaviour
{
    /// <summary>
    /// 첫 번째 라인 오브젝트의 위치를 저장하기 위한 변수
    /// </summary>
    private static float _FirstLineObjectPosition;

    [Header("라인 오브젝트")]
    public List<LineObject> m_LineObjects;

    /// <summary>
    /// 라인 그룹 인덱스를 나타냅니다.
    /// 가장 위에 위치한 그룹이 0 번으로 사용됩니다.
    /// 해당 인덱스에 따라 라인 그룹이 배치되는 위치가 결정되도록 합니다.
    /// </summary>
    private int _LineGroupIndex;

    /// <summary>
    /// 해당 라인 그룹에서 캐릭터와 충돌되었을 때 
    /// 케릭러가 통과 할 수 있는 색상 타입을 나타냅니다.
    /// </summary>
    private ColorType _PassableColor;

    /// <summary>
    /// 라인 통과시 플레이어에게 설정시킬 색상을 나타냅니다.
    /// </summary>
    private Color _NextColor;

    /// <summary>
    /// 플레이어 캐릭터 객체를 나타냅니다.
    /// </summary>
    private PlayerCharacter _PlayerCharacter;

    /// <summary>
    /// 라인 그룹 통과 시 발생시킬 이벤트입니다.
    /// </summary>
    private System.Action<int, Color, bool> _OnLineGroupPassed;

    private void FixedUpdate()
    {
        // 라인 그룹 위치 이동
        MoveLineGroup();

        // 라인 오브젝트 스크롤링
        ScrollingLine();
    }

    /// <summary>
    /// 라인 오브젝트를 스크롤링 시킵니다.
    /// </summary>
    private void ScrollingLine()
    {
        foreach(LineObject lineObject in m_LineObjects) 
        {
            // 라인 위치를 얻습니다.
            Vector3 linePosition = lineObject.transform.position;

            // 오른쪽으로 이동시킵니다.
            linePosition.x += Constants.LINE_SCROLL_SPEED;

            if(linePosition.x > Constants.LINE_FINISH_POS_X)
            {
                // 라인 오브젝트 스크롤링
                linePosition.x -= Constants.NUMBER_OF_LINEOBJECT * Constants.LINE_OBJECT_WIDTH;
            }

            // 변경한 위치를 적용합니다.
            lineObject.transform.position = linePosition;
        }

        // 다른 라인과 위치를 동기화 시키기 위해 
        // 첫 번째 라인 그룹안 경우에만 첫 번째 라인 오브젝트의 위치를 저장합니다.
        if(_LineGroupIndex == 0)
        {
            _FirstLineObjectPosition = m_LineObjects[0].transform.localPosition.x;
        }
    }


    /// <summary>
    /// 라인 그룹 위치를 이동시킵니다.
    /// </summary>
    private void MoveLineGroup()
    {
        // 목표 Y 위치를 계산합니다.
        float targetYPosition = Constants.LINEGROUP_TERM_Y * _LineGroupIndex;

        // 현재 위치를 얻습니다.
        Vector3 currentPosition = transform.position;

        // 목표 위치를 계산합니다.
        Vector3 targetPostion = Vector3.down * targetYPosition;

        // 선형 보간을 이용하여 부드럽게 이동시킵니다.
        currentPosition = Vector3.Lerp(currentPosition, targetPostion, 0.05f);

        // 위치를 적용합니다.
        transform.position = currentPosition;
    }

    /// <summary>
    /// 라인 그룹 내의 라인 오브젝트들을 모두 초기화합니다.
    /// </summary>
    /// <param name="colors">사용 가능한 색상들을 전달합니다.</param>
    /// <param name="colorTypes">사용될 색상 타입을 순서대로 전달합니다.</param>
    private void InitializeLineObject(List<Color> colors, List<ColorType> colorTypes)
    {
        for (int i = 0; i < colorTypes.Count; ++i)
        {
            // 적용시킬 라인 오브젝트
            LineObject lineObject = m_LineObjects[i];

            // 적용될 색상 타입
            ColorType colorType = colorTypes[i];

            // 설정시킬 색상을 얻습니다.
            Color lineColor = colors[(int)colorType];

            // 라인 오브젝트 초기화
            lineObject.InitializeLineObject(
                lineColor,
                _PassableColor == colorType,
                OnCharacterOverlapped);

            // 라인 오브젝트 위치 동기화
            lineObject.transform.localPosition =
                Vector3.right * (_FirstLineObjectPosition + i * Constants.LINE_OBJECT_WIDTH);
        }
    }

    /// <summary>
    /// 라인 그룹을 초기화합니다.
    /// </summary>
    /// <param name="playerCharacter">플레이어 캐릭터를 전달합니다.</param>
    /// <param name="lineGroupIndex">설정시킬 라인 그룹 인덱스를 전달합니다.</param>
    /// <param name="colors">라인 그룹에서 사용할 수 있는 색상 리스트를 전달합니다.</param>
    /// <param name="colorTypes">라인 그룹에서 사용될 색상 타입 배열을 전달합니다.</param>
    /// <param name="passableColor">정답 색상을 전달합니다.</param>
    /// <param name="nextColor">이 라인을 통과한 후 플레이어에게 설정시킬 색상을 전달합니다.</param>
    public void InitializeLineGroup(
        PlayerCharacter playerCharacter,
        int lineGroupIndex,
        List<Color> colors,
        List<ColorType> colorTypes,
        ColorType passableColor,
        ColorType nextColor,
        System.Action<int, Color, bool> onLineGroupPassed)
    {
        _OnLineGroupPassed = onLineGroupPassed;
        _PlayerCharacter = playerCharacter;
        _PassableColor = passableColor;
        _NextColor = colors[(int)nextColor];

        // 라인 그룹 인덱스 설정
        SetLineGroupIndex(lineGroupIndex);

        // 라인 오브젝트 초기화
        InitializeLineObject(colors, colorTypes);
    }

    /// <summary>
    /// 라인 그룹 인덱스를 설정합니다.
    /// </summary>
    /// <param name="lineGroupIndex">설정시킬 인덱스를 전달합니다.</param>
    public void SetLineGroupIndex(int lineGroupIndex)
    {
        _LineGroupIndex = lineGroupIndex;
    }

    /// <summary>
    /// 플레이어가 이 라인 그룹과 충돌이 발생한 경우 호출되는 메서드입니다.
    /// </summary>
    /// <param name="isPassableLine">통과 가능 여부가 전달됩니다.</param>
    private void OnCharacterOverlapped(bool isPassableLine)
    {
        // 통과 가능한 라인 오브젝트와 충돌한 경우 점프 시킵니다,
        if(isPassableLine) 
        {
            // 점프
            _PlayerCharacter.Jump();
        }

        _OnLineGroupPassed?.Invoke(_LineGroupIndex, _NextColor, !isPassableLine);
    }
}
