using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 플레이어 캐릭터를 나타내기 위한 컴포넌트 입니다.
/// </summary>
public sealed class PlayerCharacter : MonoBehaviour
{
    /// <summary>
    /// 사용될 메터리얼 에셋을 나타냅니다.
    /// Awake() 에서 동기 방식으로 로드됩니다.
    /// </summary>
    private static Material _MaterialAsset;

    [Header("Game UI")]
    public GameSceneUI m_GameSceneUI;

    [Header("점프 힘")]
    public float m_JumpPower = 0.2f;

    [Header("중력에 곱해질 승수")]
    public float m_GravityMultiplier = 0.001f;

    /// <summary>
    /// 캐릭터에 사용될 Y 속도입니다.
    /// </summary>
    private float _YVelocity;

    /// <summary>
    /// MeshRenderer Component 를 나타냅니다.
    /// </summary>
    private MeshRenderer _MeshRenderer;

    /// <summary>
    /// 복사 생성된 메터리얼 객체를 나타냅니다.
    /// </summary>
    private Material _MaterialInstance;

    /// <summary>
    /// 게임이 시작되었음을 나타냅니다.
    /// </summary>
    private bool _IsGameStarted;


    private void Awake()
    {
        if(!_MaterialAsset)
        {
            _MaterialAsset = Resources.Load<Material>("M_CustomLit");
        }


        // FixedUpdate 메서드 호출 주기를 설정합니다.
        Time.fixedDeltaTime = 1 / 60.0f;

        _MeshRenderer = GetComponentInChildren<MeshRenderer>();
        _MeshRenderer.material = _MaterialInstance = Instantiate(_MaterialAsset);

    }

    private void Update()
    {
        // 점프 입력 처리
        if(Input.GetMouseButtonDown(0) ||
            Input.GetKeyDown(KeyCode.Space))
        {
            if(!_IsGameStarted)
            {
                _IsGameStarted = true;
                m_GameSceneUI.OnGameStart();
            }

            // 점프
            Jump();
        }
    }

    private void FixedUpdate()
    {
        // 중력을 계산합니다.
        ApplyGravity();

        // 속도에 따라 캐릭터를 이동시킵니다.
        Move();
    }

    /// <summary>
    /// 중력을 계산합니다.
    /// </summary>
    private void ApplyGravity()
    {
        if (!_IsGameStarted) return;

        // 이동에 연산 될 엔진 중력값을 얻습니다.
        float engineGravity = Physics.gravity.y * m_GravityMultiplier;

        // 하강 속도를 증가시킵니다.
        _YVelocity += engineGravity;
        
    }

    private void Move()
    {
        transform.position += Vector3.up * _YVelocity;
    }

    /// <summary>
    /// 점프 시킵니다.
    /// </summary>
    public void Jump()
    {
        _YVelocity = m_JumpPower;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="color"></param>
    public void SetColor(Color color)
    {
        _MaterialInstance.SetColor(Constants.MATPARAM_BASECOLOR,color);
    }

    public void OnLineGroupPassed()
    {
        ++GameManager.getInstance.m_ScoreData.m_Score;
        
        m_GameSceneUI.UpdateScore();
    }

    /// <summary>
    /// 게임 오버 시 호출되는 메서드입니다.
    /// </summary>
    public void OnGameOver()
    {
        Time.timeScale = 0.0f;
        m_GameSceneUI.OnGameOver();
    }
}
