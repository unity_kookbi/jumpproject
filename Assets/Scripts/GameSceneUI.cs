using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameSceneUI : MonoBehaviour
{
    [Header("터치시 게임 시작 텍스트")]
    public TMP_Text m_TouchToPlayText;

    [Header("점수 텍스트")]
    public TMP_Text m_ScoreText;

    [Header("메인신 버튼")]
    public Button m_GoToMainSceneButton;

    private void Awake()
    {
        m_GoToMainSceneButton.onClick.AddListener(OnGoToMainButtonClicked);

        m_GoToMainSceneButton.gameObject.SetActive(false);
    }

    private void OnGoToMainButtonClicked()
    {
        Time.timeScale = 1.0f;

        // 최고 점수 갱신
        GameManager.getInstance.UpdateBestScore();

        SceneManager.LoadScene("MainScene");
    }

    public void OnGameStart()
    {
        // 텍스트 비활성화
        m_TouchToPlayText.gameObject.SetActive(false);
    }

    public void OnGameOver()
    {
        // 버튼 활성화
        m_GoToMainSceneButton.gameObject.SetActive(true);
    }

    public void UpdateScore()
    {
        m_ScoreText.text = GameManager.getInstance.m_ScoreData.m_Score.ToString();
    }
}
