using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class LineGenerator : MonoBehaviour
{
    [Header("플레이어 캐릭터 객체")]
    public PlayerCharacter m_PlayerCharacter;

    [Header("사용될 색상 데이터 에셋")]
    public ColorData m_ColorData;

    [Header("라인 그룹 프리팹")]
    public LineGroup m_LineGroupPrefab;

    [Header("시작 색상")]
    public ColorType m_StartColor;

    /// <summary>
    /// 생성된 라인 그룹 객체들을 담습니다.
    /// </summary>
    private List<LineGroup> _GeneratedLineGroups;

    /// <summary>
    /// 생성된 라인의 정답 색상을 저장시킬 변수입니다.
    /// 생성되는 다음 라인에서 해당 색상을 포함시키기 위해 사용됩니다.
    /// White 색상부터 시작되기 때문에 첫 라인에 White 색상을 포함시키기 위해 초기값을 다음으로 설정합니다.
    /// </summary>
    private ColorType _NextPassableColor = ColorType.White;

    private void Start()
    {
        m_StartColor = (ColorType)Random.Range(0, 6);
        // 시작 색상을 적용시킵니다.
        _NextPassableColor = m_StartColor;

        // 리스트 메모리 할당
        _GeneratedLineGroups = new();

        // MAX_LINEGROUP_COUNT 만큼 라인 그룹 객체를 생성합니다.
        for (int i = 0; i < Constants.MAX_LINEGROUP_COUNT; ++i)
        {
            _GeneratedLineGroups.Add(GenerateLineGroup(i));
        }

        // 캐릭터에게 적용시킬 색상을 얻습니다.
        Color startColor =
            m_ColorData.colors.Find(
            (ColorData_Element elem) => elem.colorType == m_StartColor).color;

        // 캐릭터에게 색상을 적용합니다.
        m_PlayerCharacter.SetColor(startColor);
    }

    /// <summary>
    /// 라인 그룹을 생성합니다.
    /// </summary>
    /// <param name="lineGroupIndex">라인 그룹 인덱스를 전달합니다.</param>
    /// <returns></returns>
    private LineGroup GenerateLineGroup(int lineGroupIndex)
    {
        List<Color> colors = new();

        foreach (ColorData_Element colorData in m_ColorData.colors)
        {
            colors.Add(colorData.color);
        }

        // 라인 그룹 객체를 복사 생성합니다.
        LineGroup newLineGroup = Instantiate(m_LineGroupPrefab);

        // 생성시킬 라인 그룹에 포함시킬 색상
        ColorType inclusiveColor = _NextPassableColor;

        // 다음 색상을 랜덤하게 설정합니다.
        _NextPassableColor = GetRandomColorType();

        // 라인 그룹 초기화
        newLineGroup.InitializeLineGroup(
            playerCharacter: m_PlayerCharacter,
            lineGroupIndex: lineGroupIndex,
            colors: colors,
            colorTypes: new List<ColorType>(GetSuffledColorTypeArray(inclusiveColor)),
            passableColor: inclusiveColor,
            nextColor: _NextPassableColor,
            onLineGroupPassed: OnLineGroupPasssed);

        // 생성된 라인 그룹 객체를 반환합니다.
        return newLineGroup;
    }

    /// <summary>
    /// 랜덤한 색상 타입을 반환하는 메서드
    /// </summary>
    /// <returns></returns>
    private ColorType GetRandomColorType()
    {
        // 색상 타입 열거 형식을 배열로 얻습니다.
        ColorType[] colorTypeArray = System.Enum.GetValues(typeof(ColorType)) as ColorType[];

        int randomIndex = Random.Range(0, colorTypeArray.Length);

        // 랜덤한 요소를 반환합니다.
        return colorTypeArray[randomIndex];

    }


    /// <summary>
    /// 라인 그룹에서 사용될 랜덤하게 섞인 색상타입 배열을 반환합니다.
    /// </summary>
    /// <param name="inclusiveColor">배열에 포함시킬 색상을 전달합니다.</param>
    /// <returns>inclusiveColor 가 포함된 섞인 색상 배열을 반환합니다.</returns>
    private ColorType[] GetSuffledColorTypeArray(ColorType inclusiveColor)
    {
        void Swap(ref ColorType target1, ref ColorType target2)
        {
            ColorType temp = target1;
            target1 = target2;
            target2 = temp;
        }

        // 색상 타입 열거 형식을 배열로 얻습니다.
        ColorType[] colorTypeArray = System.Enum.GetValues(typeof(ColorType)) as ColorType[];

        #region 모든 색상 타입 요소들을 섞습니다.
        // 대상 1을 선택시키기 위한 for 문
        for (int i = 0; i < colorTypeArray.Length - 1; ++i)
        {
            // 대상 2의 인덱스를 설정합니다.
            // (대상 1 다음 요소부터 배열의 마지막 요소까지의 범위 중 하나를 선택)
            int swapTargetIndex = Random.Range(i + 1, colorTypeArray.Length);

            // 대상 1과 대상 2를 교체합니다.
            Swap(ref colorTypeArray[i], ref colorTypeArray[swapTargetIndex]);
        }
        #endregion

        #region 포함시킬 색상 요소의 인덱스를 찾습니다.
        int inclusiveColorIndex = 0;
        for (int i = 0; i < colorTypeArray.Length; ++i)
        {
            // 포함 시킬 색상 요소를 찾은 경우
            if (colorTypeArray[i] == inclusiveColor)
            {
                // 인덱스를 잠시 저장합니다.
                inclusiveColorIndex = i;
                break;
            }
        }
        #endregion

        #region 포함되어야 하는 색상을 반환시킬 배열 범위 내에 포함시킵니다.
        // 반환될 배열 범위 내에 포함시킬 색상이 담겨있지 않은 경우
        if (inclusiveColorIndex >= Constants.NUMBER_OF_LINEOBJECT)
        {
            // 배열에 포함시킬 값의 위치를 결정시킬 인덱스 변수
            int randomIndex = Random.Range(0, Constants.NUMBER_OF_LINEOBJECT);

            Swap(ref colorTypeArray[inclusiveColorIndex], ref colorTypeArray[randomIndex]);
        }
        #endregion

        #region 반환시킬 배열을 생성 합니다.
        ColorType[] returnValue = new ColorType[Constants.NUMBER_OF_LINEOBJECT];

        for (int i = 0; i < returnValue.Length; ++i)
        {
            returnValue[i] = colorTypeArray[i];
        }
        #endregion

        // 5개의 요소를 담는 inclusiveColor 가 포함된 색상 타입 배열을 반환합니다.
        return returnValue;
    }


    /// <summary>
    /// 플레이어가 라인을 통과한 경우 호출되는 메서드입니다.
    /// </summary>
    /// <param name="passedLineIndex">통과한 라인 인덱스가 전달됩니다.</param>
    /// <param name="nextColor">캐릭터에게 적용시킬 색상이 전달됩니다.</param>
    /// <param name="isGameOver">게임 오버 여부가 전달됩니다.</param>
    private void OnLineGroupPasssed(int passedLineIndex, Color nextColor, bool isGameOver)
    {
        if (isGameOver)
        {
            m_PlayerCharacter.OnGameOver();
        }
        else
        {
            // 라인 그룹 제거
            PopLineGroup(passedLineIndex);

            // 라인 그룹 추가
            PushLineGroup();

            m_PlayerCharacter.SetColor(nextColor);

            m_PlayerCharacter.OnLineGroupPassed();
        }
    }

    /// <summary>
    /// 라인 그룹을 배열에서 뺍니다
    /// </summary>
    /// <param name="lineGroupIndex">제거할 라인 그룹 인덱스를 전달합니다.</param>
    private void PopLineGroup(int lineGroupIndex)
    {
        // 통과한 라인 그룹 객체를 얻습니다.
        LineGroup passedLineGroup = _GeneratedLineGroups[lineGroupIndex];

        // 통과한 라인 그룹을 배열에서 제외합니다.
        _GeneratedLineGroups[lineGroupIndex] = null;

        // 빈 공간을 탐색합니다.
        for (int i = 0; i < _GeneratedLineGroups.Count - 1; ++i)
        {
            // 빈 공간을 발견한 경우
            if (_GeneratedLineGroups[i] == null)
            {
                // 가장 가까운 다음 요소를 찾고, 빈 공간과 위치를 교환합니다.
                for (int j = (i + 1); j < _GeneratedLineGroups.Count; ++j)
                {
                    // 비어있지 않은 라인 그룹 객체를 발견한 경우
                    if (_GeneratedLineGroups[j] != null)
                    {
                        // 서로 위치를 교환합니다.
                        _GeneratedLineGroups[i] = _GeneratedLineGroups[j];
                        _GeneratedLineGroups[j] = null;

                        // 빈 공간과 교체된 라인 그룹의 인덱스를 설정합니다.
                        _GeneratedLineGroups[i].SetLineGroupIndex(i);

                        // 빈 공간과 교체가 끝났으므로 다음 교환을 진행합니다.
                        break;

                    }
                }
            }

        }

        // 통과한 라인 그룹을 제거합니다.
        Destroy(passedLineGroup.gameObject);
    }

    /// <summary>
    /// 라인 그룹을 추가합니다.
    /// </summary>
    private void PushLineGroup()
    {
        // 배열의 빈 공간의 인덱스를 나타내기 위한 변수
        int emptyIndex = GetEmptyLineGroupIndex();

        // 빈 공간을 찾지 못한 경우 추가 취소
        if (emptyIndex == -1) return;

        // 라인 그룹을 생성합니다.
        _GeneratedLineGroups[emptyIndex] = GenerateLineGroup(emptyIndex);
    }

    /// <summary>
    /// 배열에서 라인 그룹을 생성할 수 있는 빈 요소 인덱스를 찾아 반환합니다.
    /// </summary>
    /// <returns>빈 요소의 인덱스를 반환합니다. 찾지 못한 경우 -1 을 반환합니다.</returns>
    private int GetEmptyLineGroupIndex()
    {
        // 배열에서 빈 공간을 찾습니다.
        for(int i = (Constants.MAX_LINEGROUP_COUNT - 1); i > -1; --i)
        {
            // i 번째 요소가 비어있다면, i 를 반환합니다.
            if (_GeneratedLineGroups[i] == null) return i;
        }
        
        return -1;

    }
}
