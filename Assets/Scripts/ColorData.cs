using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ColorData", menuName = "ScriptableObject/ColorData")]
public sealed class ColorData : ScriptableObject
{
    // ScriptableObject 상속 : 스크립터블 오브젝트를 생성하기 위한 형식으로 설정합니다.

    public List<ColorData_Element> colors;
}

/// <summary>
/// ColorData 에서 사용될 리스트 요소 클래스입니다.
/// </summary>
[System.Serializable]
public class ColorData_Element
{
    public ColorType colorType;
    public Color color;
}
// Serializable
// 지정한 클래스나 구조체를 직렬화 시킵니다.
// 특정한 형식의 내용을 저장, 전송하기 위해서 사용됩니다.