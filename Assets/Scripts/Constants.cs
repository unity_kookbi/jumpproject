

public class Constants
{
    /// <summary>
    /// 라인 스크롤링 속력
    /// </summary>
    public const float LINE_SCROLL_SPEED = 0.05f;

    /// <summary>
    /// 라인 오브젝트 시작 위치
    /// </summary>
    public const float LINE_START_POS_X = -8.0f;

    /// <summary>
    /// 라인 오브젝트 끝 위치
    /// </summary>
    public const float LINE_FINISH_POS_X = 8.0f;

    /// <summary>
    /// 라인 그룹에 사용되는 라인 오브젝트 개수
    /// </summary>
    public const int NUMBER_OF_LINEOBJECT = 5;

    /// <summary>
    /// 라인 오브젝트 너비
    /// </summary>
    public const float LINE_OBJECT_WIDTH = 4.0f;

    /// <summary>
    /// 생성시켜둘 LINEGROUP 개수
    /// </summary>
    public const int MAX_LINEGROUP_COUNT = 10;

    /// <summary>
    /// 라인 그룹 Y 위치 텀
    /// </summary>
    public const float LINEGROUP_TERM_Y = 0.5f;

    /// <summary>
    /// 메터리얼 색상 파라미터 이름
    /// </summary>
    public const string MATPARAM_BASECOLOR = "_BaseColor";

    /// <summary>
    /// 저장 파일 경로를 나타냅니다.
    /// </summary>
    public static string SAVE_DATA_PATH =>
        $"{UnityEngine.Application.dataPath}\\Resources\\SaveData\\";

    /// <summary>
    /// 저장 파일 이름입니다.
    /// </summary>
    public static string SAVE_DATA_FILENAME =>
        $"SaveData.json";
}