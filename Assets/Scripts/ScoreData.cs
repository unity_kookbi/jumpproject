using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 점수를 나타낼 수 있는 형식입니다.
/// </summary>
[System.Serializable]
public class ScoreData
{
    /// <summary>
    /// 점수입니다.
    /// </summary>
    public int m_Score;

    public ScoreData()
    {
        m_Score = 0;
    }
}
