using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// 하나의 라인 오브젝트를 나타내기 위한 컴포넌트입니다.
/// </summary>
public sealed class LineObject : MonoBehaviour
{
    /// <summary>
    /// 라인 오브젝트에서 사용될 메터리얼 에셋입니다.
    /// </summary>
    private static Material _MaterialAsset;

    /// <summary>
    /// 라인 오브젝트를 나타내는 MeshRenderer Component입니다.
    /// </summary>
    private MeshRenderer _MeshRenderer;

    /// <summary>
    /// 통과 가능 여부를 나타냅니다.
    /// </summary>
    private bool _IsPassable;

    /// <summary>
    /// 플레이어와 겹쳤을 경우 발생시킬 이벤트를 나타냅니다.
    /// </summary>
    private System.Action<bool> _OnPlayerOverlapped;

    private void Awake()
    {
        // 메터리얼 에셋이 로드되지 않았을 경우 로드합니다.
        if(!_MaterialAsset) 
        {
            // Resources/M_CustomLit 에셋을 로드합니다.
            _MaterialAsset = Resources.Load<Material>("M_CustomLit");
        }

        // MeshRenderer 컴포넌트를 찾습니다.
        _MeshRenderer = GetComponent<MeshRenderer>();
        // GetComponent<Type>() : 이 컴포넌트가 추가된 오브젝트 내부에서 Type 형식의 컴포넌트를 찾아 반환합니다.
    }


    /// <summary>
    /// 라인 오브젝트를 초기화합니다.
    /// </summary>
    /// <param name="lineColor">이 라인 오브젝트에 설정시킬 색상을 전달합니다.</param>
    /// <param name="isPassable">통과 가능 여부를 전달합니다.</param>
    /// <param name="onPlayerOverlappedEvent">플레이어 통과 시 발생시킬 이벤트를 전달합니다.</param>
    public void InitializeLineObject(
        Color lineColor, 
        bool isPassable,
        System.Action<bool> onPlayerOverlappedEvent)
    {
        _IsPassable = isPassable;
        _OnPlayerOverlapped = onPlayerOverlappedEvent;

        // 메터리얼 복사생성
        Material newMaterialInstance = Instantiate(_MaterialAsset);

        // 복사 생성 확인을 위한 메터리얼 이름 변경
        newMaterialInstance.name = "NewMaterialInstance";

        // 전달된 색상을 메터리얼 파라미터에 설정합니다.
        newMaterialInstance.SetColor(Constants.MATPARAM_BASECOLOR, lineColor);

        // 첫 번째 메터리얼을 복사 생성시킨 메터리얼로 지정합니다.
        _MeshRenderer.material = newMaterialInstance;

    }

    private void OnCollisionEnter(Collision collision)
    {
        // 플레이어 겹침 이벤트를 발생
        _OnPlayerOverlapped?.Invoke(_IsPassable);
    }
}
